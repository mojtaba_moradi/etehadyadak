import reducer from "../../_context/reducer";
import { useContext, useEffect } from "react";
import { useRouter } from "next/router";
import WithErrorHandler from "../../panelAdmin/utils/adminHoc/WithErrorHandler";

const ContextCheck = (props) => {
  const router = useRouter();

  const Context = reducer.webSiteReducer;
  const shoppingCartContextData = useContext(Context.shoppingCartReducerContext);
  const authenticationContextData = useContext(Context.authenticationReducerContext);
  // console.log({ authenticationContextData, shoppingCartContextData });
  const { dispatch: shoppingCartDispatch, state: shoppingCartState } = shoppingCartContextData;
  const { dispatch: authenticationDispatch, state: authenticationState } = authenticationContextData;
  // console.log({ authenticationDispatch, authenticationState });
  useEffect(() => {
    if (JSON.parse(localStorage.getItem("pernyBasket"))?.shoppingCart?.length) shoppingCartDispatch.newState(JSON.parse(localStorage.getItem("pernyBasket")));
  }, [router]);
  useEffect(() => {
    shoppingCartDispatch.totalPrice();
    shoppingCartDispatch.totalGoods();
  }, [shoppingCartState.shoppingCart]);
  useEffect(() => {
    authenticationDispatch.checkLogin();
  }, []);
  // }, [authenticationState.authentic]);
  return props.children;
};

export default WithErrorHandler(ContextCheck);
