import React from "react";
import Link from "next/link";
import LazyImage from "../../../../../components/LazyImage";

const MoreProductsCard = (props) => {
  const { images, name, _id } = props;
  return (
    <React.Fragment>
      <li>
        <figure>
          {/* <img src={image} alt="as" /> */}
          <LazyImage src={images[0]} alt={name} />
          <figcaption>
            <Link href={`/product?productId=${_id}`}>
              <a className="details-btn">{"جزئیات"}</a>
            </Link>
          </figcaption>
        </figure>
      </li>
    </React.Fragment>
  );
};

export default MoreProductsCard;
