import SliderContainer from "../../baseComponent/Slider/SliderContainer";
import CategoriesContainer from "./Categories/CategoriesContainer";
import ShippingDetailsCard from "./ShippingDetails/ShippingDetailsCard";
import MoreProductsContainer from "./MoreProducts/MoreProductsContainer";

const HomeScreen = ({ resData }) => {
  return (
    <div>
      <div className="base-container">
        {resData?.sliders ? <SliderContainer data={resData?.sliders} /> : ""}
        {resData?.categories ? <CategoriesContainer data={resData?.categories} /> : ""}
        <ShippingDetailsCard />
        <MoreProductsContainer products={resData?.products} banner={resData?.sliders?.length && resData?.sliders[0]} />
      </div>
    </div>
  );
};

export default HomeScreen;
