const ModalContent = () => {
  return (
    <div className="container-form">
      <form action="">
        <label htmlFor="fname">نام</label>
        <input type="text" id="fname" name="firstname" placeholder="محمدرضا" />

        <label htmlFor="lname">نام حانوادگی</label>
        <input type="text" id="lname" name="lastname" placeholder="معینی فر" />

        <label htmlFor="state">استان</label>
        <select id="state" name="state">
          <option value="خوزستان">خوزستان</option>
          <option value="گیلان">گیلان</option>
          <option value="فارس">فارس</option>
        </select>

        <label htmlFor="city">شهر</label>
        <select id="city" name="city">
          <option value="آبادان">آبادان</option>
          <option value="رشت">رشت</option>
          <option value="شیراز">شیراز</option>
        </select>

        <label htmlFor="adrress">آدرس</label>
        <textarea
          id="adrress"
          name="adrress"
          placeholder="خیابان رشتیان ، کوچه54 ، طبقه دوم، پلاک 5"
        ></textarea>

        <input type="submit" value="ادامه" />
      </form>
    </div>
  );
};

export default ModalContent;
