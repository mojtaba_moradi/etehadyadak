import Link from "next/link";
import AddButton from "../../../component/ui/AddButton";
import SidebarShoppingCart from "./SidebarShoppingCart";
import formatMoney from "../../../../panelAdmin/utils/formatMoney";
import DiscountPrice from "../../../component/DiscountPrice";
import Swal from "sweetalert2";
const CartMainContent = ({ click, dispatch, state }) => {
  const swalTrueFalse = (id, index) => {
    Swal.fire({
      title: state?.shoppingCart[index].name,
      text: "آیا مطمئن به حذف این محصول از سبد خود هستید!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "بله, حذف میکنم!",
      cancelButtonText: "خیر",
    }).then((result) => {
      console.log({ swal: id });
      if (result.value) {
        dispatch.removeProduct(id);
        Swal.fire({
          position: "top-start",
          icon: "success",
          title: "با موفقیت از سبد خرید شما حذف شد",
          showConfirmButton: false,
          timer: 2000,
        });
      }
    });
  };
  return (
    <section className="shopping-cart base-container">
      <div className="left-side-item-pictures">
        <div className="items">
          <ul>
            {state?.shoppingCart?.map((basket, index) => {
              return (
                <li key={` shoppingCart-${index}`}>
                  <figure>
                    <figcaption>
                      <div className="show-cart-product-details">
                        <div className="show-cart-product-information">
                          <div>
                            {" "}
                            <img src={basket?.images?.length && basket?.images[0]} alt="" />
                          </div>
                          <div>
                            <p>{basket?.name}</p>
                            <div className="product-prices">
                              <span>قیمت:</span>
                              <div className="price">
                                <DiscountPrice column {...{ realPrice: basket?.realPrice, newPrice: basket?.newPrice, discount: basket?.discount }} />
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="Settings">
                          <span onClick={() => dispatch.increaseOrder(basket._id)}>
                            <i className="fal fa-plus"></i>
                          </span>
                          <span className="order-number">{basket?.orderNumber || 1}</span>
                          <span onClick={() => dispatch.decreaseOrder(basket._id)} className="window-minimize">
                            <i className="far fa-window-minimize"></i>
                          </span>
                          <div className="pointer remove-icon">
                            <i onClick={() => swalTrueFalse(basket?._id, index)} title="حذف " className="fad fa-trash-alt"></i>
                          </div>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
      <SidebarShoppingCart {...{ dispatch, state }} />
    </section>
  );
};

export default CartMainContent;
