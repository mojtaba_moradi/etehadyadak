import ProductDetails from "./ProductDetails";
import ProductNav from "./ProductNav";

const ProductScreen = ({ resData }) => {
  return (
    <div className="base-container">
      <section className="product">
        {/* ========================= add to shopping cart , show product */}
        <ProductDetails data={resData} />
        {/* ========================= add comment , show comment , description */}
        <ProductNav data={resData} />
      </section>
    </div>
  );
};

export default ProductScreen;
