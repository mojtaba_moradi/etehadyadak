import React, { useEffect, useState } from "react";
import Star from "../../../../panelAdmin/component/UI/Rating/Star";
import website from "../../..";
import toastify from "../../../../panelAdmin/utils/toastify";
import ButtonComponent from "../../../component/ui/ButtonComponent";
import jalaliDateConvert from "../../../../panelAdmin/utils/jalaliDateConvert";
import AddButton from "../../../component/ui/AddButton";
import swal from "sweetalert";
const ProductReview = ({ data, setShowModal }) => {
  // const [comments, setComments] = useState([]);
  const [newComment, setNewComment] = useState({ name: "", content: "", rate: "2.5", parentType: "Product", parent: data?._id });
  // console.log({ data });
  const [userLogin, setUserLogin] = useState(false);
  const authorization = website.utils.authorization();
  useEffect(() => {
    if (authorization) setUserLogin(true);
    else setUserLogin(false);
  }, [authorization]);
  const changeStateValue = ({ name, value }) => {
    setNewComment({ ...newComment, [name]: value });
  };
  // console.log({ newComment });
  const submitNewComment = async (e) => {
    e.preventDefault();
    if (await website.userApi.post.globalPostApi(website.values.apiString.COMMENT, newComment)) {
      // toastify("با موفقیت ثبت شد", "success");
      swal({
        title: "نظر شما با موفقیت ثبت شد!",
        text: "کارشناسان در حال بررسی پیامتان هستند بعد از تایید نظر شما ثبت خواهد شد!",
        icon: "success",
        button: "تایید",
      });
      setNewComment({ name: "", content: "", rate: "2.5", parentType: "Product", parent: newComment?.parent });
    }
  };
  const userComment = () => {
    return data?.comments?.map((comment) => {
      // console.log({ comment: jalaliDateConvert(comment?.createdAt) });
      let dateAndClock = jalaliDateConvert(comment?.createdAt, true);
      return (
        <div key={comment?.createdAt} className="show-comments-wrapper">
          <span className="textEnd comment-date">
            {" "}
            {dateAndClock.clock} - {dateAndClock.date}
          </span>
          <div className="comment-wrapper">
            <div>
              <span> {comment?.name}</span>

              <div className="comment-rate">
                <Star rating={comment?.rate} fixed />
              </div>
            </div>
            <div className="show-comment-text">
              <p>{comment?.content}</p>
            </div>
          </div>
        </div>
      );
    });
  };
  return (
    <div className="product-description-review">
      <form className="review-form1">
        <div>
          <h3 className="users-comments-subtitle">{"نظرات کاربران"}</h3>
          {userComment()}
          <h4 className="users-add-comment-subtitle">نظر خود را با ما به اشتراک بگذارید</h4>
          {userLogin ? (
            <form onSubmit={submitNewComment} className="review-form2" action="review">
              <div className="show-review">
                <label htmlFor="name">{"نام شما"}</label>
                <input placeholder={"نام شما"} value={newComment.name} required id="name" type="" onChange={(e) => changeStateValue({ value: e.target.value, name: "name" })} />
                <label htmlFor="message">{"متن نظر شما"}</label>
                <textarea placeholder={"متن نظر شما"} value={newComment.content} onChange={(e) => changeStateValue({ value: e.target.value, name: "content" })} required id="message" cols="30" rows="10"></textarea>
              </div>
              <div className="rating-section">
                <label>امتیاز</label>
                <div className="star-rating-wrapper">
                  <Star rating={newComment.rate} change={(rate) => changeStateValue({ value: rate, name: "rate" })} />
                </div>
              </div>
              {/* <button>ارسال</button> */}
              <div className="textEnd">
                <AddButton title={"ثبت نظر شما"} color={"orange"} />
              </div>
            </form>
          ) : (
            <div className="user-check-active-road">
              <span>{"برای ارسال نظر باید اول ثبت نام کرده یا به پنل کاربری خود وارد شوید !!"}</span>
              <ButtonComponent title={"ورود / ثبت نام"} bgColor={"green"} style={null} click={() => setShowModal(true)} />
            </div>
          )}
        </div>
      </form>
    </div>
  );
};
export default ProductReview;
