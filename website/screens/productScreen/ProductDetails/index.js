import React, { useState, useEffect, useContext } from "react";
import ProductSlider from "../../../reusableComponent/ProductsSlider";
import Star from "../../../../panelAdmin/component/UI/Rating/Star";
import DiscountPrice from "../../../component/DiscountPrice";
import AddButton from "../../../component/ui/AddButton";
import reducer from "../../../../_context/reducer";

const ProductDetails = ({ data }) => {
  // ======================== context
  const Context = reducer.webSiteReducer.shoppingCartReducerContext;
  const giveContextData = useContext(Context);
  const { dispatch, state } = giveContextData;
  const [isProduct, setIsProduct] = useState();
  // ======================== End context
  // ======================== useEffect
  const checkIsProduct = () => {
    let hasProductInBasket = state?.shoppingCart?.filter((basket) => {
      if (basket?._id === data._id) return basket;
      else return false;
    });
    setIsProduct(!!hasProductInBasket?.length);
  };
  useEffect(() => {
    checkIsProduct();
  }, [state.shoppingCart]);
  // ======================== End useEffect
  // ======================== console.log()
  // console.log({ data });
  // console.log({ giveContextData, state });
  // ======================== End console.log()

  let REMOVE_TITLE = "حذف از سبد خرید";
  let ADD_TITLE = "افزودن به سبد خرید";
  return (
    <section className="product-wraper">
      <div className="right-side">
        <div className="right-side-inner">
          <h2>{data?.name}</h2>
          <ul className="product-detail-list">
            <li>
              <span>فروشنده :</span>
              <a href="#" alt="something">
                {data?.owner?.name || "پرنی مارکت"}
              </a>
            </li>
            <li>
              <span>دسته بندی :</span>
              <strong> {data?.category?.name}</strong>
            </li>
            <li>
              <span>وزن محصول :</span>
              <strong>
                {data.weight} {data.unit}
              </strong>
            </li>
            <li className="rating-li">
              <span>امتیاز :</span>
              <Star rating={data?.rate} fixed />
            </li>
            <li>
              <span> قیمت :</span>
              <DiscountPrice column newPrice={data?.newPrice} discount={data?.discount} realPrice={data?.realPrice} />
            </li>
          </ul>

          <div className="add-wraper">
            <AddButton color={isProduct ? "gray" : "blue"} click={isProduct ? () => dispatch.removeProduct(data._id) : () => dispatch.addProduct(data)} title={!isProduct ? ADD_TITLE : REMOVE_TITLE} />
          </div>
        </div>
      </div>
      <div className="left-side">
        <ProductSlider data={data?.images} />
      </div>
    </section>
  );
};
export default ProductDetails;
