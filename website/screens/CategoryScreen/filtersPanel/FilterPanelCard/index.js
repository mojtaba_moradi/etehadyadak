import React from 'react';

const FilterPanelCard = (props) => {
  const { title } = props;
  return (
    <React.Fragment>
      <li>
        <p>{title}</p>
      </li>
    </React.Fragment>
  );
};

export default FilterPanelCard;
