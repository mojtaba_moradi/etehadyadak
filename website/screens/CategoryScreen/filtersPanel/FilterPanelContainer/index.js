import React from "react";
import FilterPanelCard from "../FilterPanelCard";

const FilterPanelContainer = ({ click, activeTitle }) => {
  // console.log({ click, activeTitle });
  const datas = [
    { title: "جدیدترین ها", id: "latest" },
    { title: "پیشنهادات ویژه", id: "offer" },
    { title: "بیشترین تخفیف ها", id: "mostDiscount" },
    { title: "ارزان ترین ها", id: "cheapest" },
    { title: "  گران ترین ها ", id: "expensive" },
  ];

  return (
    <div className="filter-items">
      <h6 className="filters-title">فیلترها</h6>
      <ul>
        {datas.map((data) => (
          <li>
            <p className={`pointer transition0-2 filterAmountTitle ${activeTitle === data.id ? "active" : ""}`} onClick={() => click({ id: data.id, name: data.id })}>
              {data.title}
            </p>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default FilterPanelContainer;
