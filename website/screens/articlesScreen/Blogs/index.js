import { useRef, useEffect } from "react";
import jalaliDateConvert from "../../../../panelAdmin/utils/jalaliDateConvert";

const { default: Link } = require("next/link");

const Blogs = ({ datas }) => {
  console.log({ mooj: datas });
  return (
    <section className="blogs">
      <ul>
        {datas?.map((data) => {
          let dateAndClock = jalaliDateConvert(data?.createdAt, true);
          // console.log({ dateAndClock });
          let content = data?.content;
          function ShowHtml(props) {
            let body = props;
            var indexStr = body.indexOf("</p>");
            let sliceStr = body.slice(0, indexStr);
            // console.log({ indexStr, sliceStr });
            return <div dangerouslySetInnerHTML={{ __html: sliceStr }} />;
          }
          // console.log({ content });
          return (
            <li>
              <Link
                // href={`/product?productInfo=${props}`}
                href={`/blogs/[id]`}
                as={`/blogs/${data?._id}`}
              >
                <a>
                  <figure>
                    <img src={data?.thumbnail} alt="" />
                    <figcaption>
                      <p>{data?.blogCategory?.name}</p>
                      <h1>{data?.title}</h1>
                      <p>{ShowHtml(content)}</p>
                      <h6>
                        {" "}
                        {dateAndClock.clock} - {dateAndClock.date}
                      </h6>
                    </figcaption>
                  </figure>
                </a>
              </Link>
            </li>
          );
        })}
      </ul>
    </section>
  );
};

export default Blogs;
