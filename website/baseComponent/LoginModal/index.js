import { useState, useRef, useContext } from "react";
import { ENTER_TO_PERNY, ENTER_CORRECT_NUMBER } from "../../values/Strings";
import { Modal } from "react-bootstrap";
import { postTest } from "../../userApi/post/postTest";
import { putApi } from "../../userApi/put";
import LoadingDot1 from "../../../panelAdmin/component/UI/Loadings/LoadingDot1";
import reducer from "../../../_context/reducer";
const LoginModal = ({ show, setShow }) => {
  // const [show, setShow] = useState(false);
  const websiteContext = reducer.webSiteReducer;
  const authentication = useContext(websiteContext.authenticationReducerContext);
  const { dispatch: authenticationDispatch, state: authenticationState } = authentication;

  const [phoneInput, setPhoneInput] = useState(true);
  const [errorTitle, setErrorTitle] = useState(null);
  const [mobileNO, setMobileNo] = useState(false);
  const [isLoading, setLoading] = useState(false);
  let mobileRef = useRef(null);
  let codeRef = useRef(null);
  let error = "";
  const handleClose = () => {
    setShow(false);
    setPhoneInput(true);
    setErrorTitle(false);
    setMobileNo(false);
  };
  // =========================== axios ==========================

  const postVerifyCode = async () => {
    // +++++++++++++++++++TO DO
    setLoading(true);

    const resVerifyPost = await putApi("/login", {
      phoneNumber: mobileNO,
      verificationCode: codeRef.current.value,
    });
    console.log({ resVerifyPost });
    if (resVerifyPost.status === 200) {
      authenticationDispatch.addToken(resVerifyPost.data.token);
      // Cookies.set("PernyUserToken", resVerifyPost.data.token, { expires: 7 });
      // setGreeting(true);
      handleClose();
    }
    setLoading(false);
  };

  const postPhoneNumber = async () => {
    setLoading(true);

    setMobileNo(mobileRef.current.value);
    if (
      await postTest("/login", {
        phoneNumber: mobileRef.current.value,
      })
    )
      setPhoneInput(false);

    setLoading(false);
  };

  // ===========================End axios ==========================
  const sendCodeHandler = (e) => {
    e.preventDefault();
    if (codeRef.current.value.length != 6) error = ERNTER_SIX_CODE;
    else postVerifyCode();
    setErrorTitle(error);
  };

  const loginHandler = (e) => {
    e.preventDefault();
    if (mobileRef.current.value.length != 11 || mobileRef.current.value[0] !== "0" || mobileRef.current.value[1] !== "9") {
      error = ENTER_CORRECT_NUMBER;
    } else {
      postPhoneNumber();
    }
    setErrorTitle(error);
  };
  const _handelChangeInput = () => {
    if (errorTitle) setErrorTitle(false);
  };
  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>{ENTER_TO_PERNY}</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div className={errorTitle ? "error" : "not-show"}>
          <h2>{errorTitle}</h2>
        </div>

        {phoneInput ? (
          <div className="login-form-wrapper">
            <form className="form-login" onSubmit={loginHandler}>
              <label htmlFor="mobile"> شماره همراه خود را وارد کنید</label>
              <div className="login-name-wrapper">
                <i className="fal fa-user"></i>
                <input onChange={_handelChangeInput} id="mobile" className="form-login-name-input" type="text" placeholder={mobileNO || "********09"} aria-label="login-name" ref={mobileRef} />
              </div>
              {/* <button disabled={isLoading} type="button" className="btns btns-add ">
                {isLoading ? <LoadingDot1 width="2em" height="1.6em" /> : "ورود"}
              </button> */}
              <button disabled={isLoading}>{isLoading ? <LoadingDot1 width="2em" height="1.6em" /> : "ادامه"}</button>
            </form>
          </div>
        ) : (
          <div className="login-form-wrapper">
            <div className="edit-phone">
              <div className="phone-num">
                <span>شماره همراه شما </span>
                <span>{mobileNO}</span>
              </div>
              <span className="span-edit" onClick={() => setPhoneInput(true)}>
                ویرایش شماره همراه
              </span>
            </div>

            <form className="form-login" onSubmit={sendCodeHandler}>
              <label htmlFor="code"> کد شش رقمی را وارد کنید</label>
              <div className="login-name-wrapper">
                <input onChange={_handelChangeInput} id="code" className="code-input" type="text" placeholder="" aria-label="login-code" ref={codeRef} />
              </div>
              <button disabled={isLoading}>{isLoading ? <LoadingDot1 width="2em" height="1.6em" /> : " ارسال کد"}</button>
            </form>
          </div>
        )}
      </Modal.Body>
    </Modal>
  );
};

export default LoginModal;
