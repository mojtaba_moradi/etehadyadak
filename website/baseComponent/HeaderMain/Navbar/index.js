import Link from "next/link";
import { useRouter } from "next/router";
import NavbarSmallDevice from "./NavbarSmallDevice";
const Navbar = ({ menuBtn, setShowSmallDeviceSearch, smallDeviceSearchHandler, openMenu, showSmallDeviceSearch, searchRef, searchOnChangeHandler, menu, searchDropDown, searchingData, MENU_DATA, userMobile, setMenuState, menuState, closeMenu }) => {
  const router = useRouter();
  return (
    <nav>
      <NavbarSmallDevice {...{ menuBtn, setShowSmallDeviceSearch, showSmallDeviceSearch, searchRef, searchOnChangeHandler, searchDropDown, searchingData, smallDeviceSearchHandler, openMenu, userMobile }} />
      <ul ref={(el) => (menu = el)} className={menuState ? "show" : "unShow"}>
        <li>
          <i onClick={closeMenu} className="far fa-times"></i>
        </li>
        {MENU_DATA(router.asPath).map((data, index) => (
          <li key={`menu-data-${index}`}>
            <Link href={data.link}>
              <a onClick={() => setMenuState(false)}>
                <h6 className={`${data.active ? "active" : ""}`}>{data.title}</h6>
              </a>
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Navbar;
