import Link from "next/link";

const NavbarSmallDevice = ({ menuBtn, setShowSmallDeviceSearch, showSmallDeviceSearch, searchRef, searchOnChangeHandler, searchDropDown, searchingData, smallDeviceSearchHandler, openMenu, userMobile }) => {
  return (
    <div className="small-device-wrapper">
      <button ref={(el) => (menuBtn = el)} onClick={openMenu} className="menu-btn">
        <i className="far fa-bars"></i>
      </button>
      <div className="small-search-wrapper">
        <button className="menu-btn" onClick={() => setShowSmallDeviceSearch(!showSmallDeviceSearch)}>
          <i className="fal fa-search"></i>
        </button>
        <div className={showSmallDeviceSearch ? "search show-search-input" : "search"}>
          <form className="form-search">
            <input className="form-search-input" type="search" placeholder="شما میتوانید محصول مورد نظر خودتان را جستجو کنید" aria-label="Search" ref={searchRef} onChange={searchOnChangeHandler} />
          </form>
          <div className={searchDropDown ? "search-drop-down show-search" : "search-drop-down"}>
            {searchingData?.data?.docs?.map((data) => {
              // console.log({ searchData: data, images: data.images[0] });
              return (
                <Link href={`/product?productId=${data._id}`}>
                  <a key={data._id} onClick={smallDeviceSearchHandler}>
                    <figure>
                      <img src={data?.images[0]} alt="" />
                      <figcaption>
                        <p>{data?.name}</p>

                        {/* <p>
                          {data?.realPrice}
                          <span> ریال</span>
                        </p> */}
                      </figcaption>
                    </figure>
                  </a>
                </Link>
              );
            })}
          </div>
        </div>
      </div>
      <button className="menu-btn">{userMobile()}</button>
    </div>
  );
};

export default NavbarSmallDevice;
