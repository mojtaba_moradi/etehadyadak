import React from "react";
import Link from "next/link";

const SliderCard = (props) => {
  const { image, parentType, parent } = props;
  console.log({ props });
  let URL = parentType.toLowerCase();
  return (
    <Link href={`${URL}/[id]`} as={`${URL}/${parent._id}`}>
      <a>
        <div className="slider-card-wrapper">
          <img src={image} alt="1" />
        </div>
      </a>
    </Link>
  );
};

export default SliderCard;
