import React from "react";
import { Carousel } from "react-responsive-carousel";
import SliderCard from "../SliderCard";
import SliderArrow from "../SliderArrow";
import SliderArrowP from "../SliderArrowP";
import header from "../../../public/images/header.jpg";

// TODO => we should connect to api and get images
const images = [header, "https://i.picsum.photos/id/243/1366/410.jpg", "https://i.picsum.photos/id/223/1366/410.jpg", "https://i.picsum.photos/id/253/1366/410.jpg"];

const SliderContainer = ({ data }) => {
  return (
    <div onDrag={(e) => e.preventDefault()} className="slider-container  my-4">
      <Carousel renderArrowNext={(onClickHandler, hasNext) => hasNext && <SliderArrow onClick={onClickHandler} />} renderArrowPrev={(onClickHandler, hasPrev) => hasPrev && <SliderArrowP onClick={onClickHandler} />} swipeable={true} transitionTime={2000} interval={5000} infiniteLoop={true} autoPlay={true} showThumbs={false} showStatus={false}>
        {data?.map((info, index) => {
          return <SliderCard {...info} key={`slider-card-${index}`} />;
        })}
      </Carousel>
    </div>
  );
};

export default SliderContainer;
