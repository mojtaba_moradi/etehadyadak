import ShoppingCartCard from "../ShoppingCartCard";
import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";

const shoppingCartItems = [
  {
    _id: "01",
    heading: "آیتم خریداری شده",
    price: "50000",
    amount: "5",
    href: "#",
  },
  {
    _id: "02",
    heading: "آیتم خریداری شده",
    price: "20000",
    amount: "1",
    href: "#",
  },
  {
    _id: "03",
    heading: "آیتم خریداری شده",
    price: "30000",
    amount: "3",
    href: "#",
  },
  {
    _id: "04",
    heading: "آیتم خریداری شده",
    price: "70000",
    amount: "7",
    href: "#",
  },
];

const ShoppingCartContainer = ({ clicked }) => {
  const store = useSelector((state) => {
    return state.cart.cartItems;
  });

  const dispatch = useDispatch();

  const [basketItem, setBasketItem] = useState(shoppingCartItems);

  const delteHandler = (id) => {
    const basket = basketItem.filter((basket) => {
      return basket._id !== id;
    });
    setBasketItem(basket);
  };
  const renderShoppigCartItems = () => {
    return basketItem.map((item) => (
      <li>
        <p>{item.heading}</p>
        <span>{item.price} تومان</span>
        <span>{item.amount}</span>
        <span className="close" onClick={() => delteHandler(item._id)}>
          <i className="far fa-times"></i>
        </span>
      </li>
    ));
  };
  return (
    <ShoppingCartCard clicked={clicked} itmes={renderShoppigCartItems()} />
  );
};
export default ShoppingCartContainer;
