import { useEffect } from 'react';

const RayChat = () => {
  const ray = () => {
    !(function () {
      function t() {
        var t = document.createElement('script');
        (t.type = 'text/javascript'),
          (t.async = !0),
          localStorage.getItem('rayToken')
            ? (t.src =
                'https://app.raychat.io/scripts/js/' +
                o +
                '?rid=' +
                localStorage.getItem('rayToken') +
                '&href=' +
                window.location.href)
            : (t.src = 'https://app.raychat.io/scripts/js/' + o);
        var e = document.getElementsByTagName('script')[0];
        e.parentNode.insertBefore(t, e);
      }
      var e = document,
        a = window,
        o = '946e8a15-6dd7-4446-aae3-b185f4a8498f';
      'complete' == e.readyState
        ? t()
        : a.attachEvent
        ? a.attachEvent('onload', t)
        : a.addEventListener('load', t, !1);
    })();
  };

  useEffect(() => {
    ray();
  }, []);
  return <div></div>;
};

export default RayChat;
