import homeScreen from "./homeScreen";
import category from "./category";
import product from "./product";
import comment from "./comment";
import blogCategory from "./blogCategory";

const get = { blogCategory, homeScreen, category, product, comment };
export default get;
