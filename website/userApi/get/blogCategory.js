import axios from "../userBaseUrl";
import website from "../..";

const blogCategory = (param) => {
  let URL = `${website.values.apiString.BLOG_CATEGORY}`;
  if (param?.id) URL += "/" + param.id;
  if (param?.page) URL += "?page=" + param.page;
  return axios
    .get(URL)
    .then((res) => {
      console.log({ res });

      return res?.data;
    })
    .catch((er) => {
      console.log({ er });

      return false;
    });
};

export default blogCategory;
