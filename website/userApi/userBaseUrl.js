// import axiosBase from "../../globalUtils/axiosBase";
import axios from "axios";
import Cookie from "js-cookie";

const instance = axios.create({ baseURL: "https://pernymarket.ir/api/v1" });
console.log({ token2: Cookie.get("PernyUserToken") });
instance.defaults.headers.common["Authorization"] = "Bearer " + Cookie.get("PernyUserToken");
export default instance;

// const UserBaseUrl = () => {
//   const Context = reducer.webSiteReducer;
//   const authenticationContextData = useContext(Context.authenticationReducerContext);
//   console.log({ authenticationContextData, shoppingCartContextData });
//   // const { dispatch, state } = authenticationContextData;
//   const instance = axios.create({ baseURL: "https://pernymarket.ir/api/v1" });

//   console.log({ token2: Cookie.get("PernyUserToken") });
//   instance.defaults.headers.common["Authorization"] = "Bearer " + Cookie.get("PernyUserToken");
//   return instance;
// };

// export default UserBaseUrl;
