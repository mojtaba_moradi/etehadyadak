import axios from "../userBaseUrl";
export const putApi = async (url, data) =>
  axios
    .put(url, data)
    .then((res) => {
      return res;
    })
    .catch((e) => {
      return e;
    });
