import axios from "../userBaseUrl";

const globalPostApi = async (url, data) => {
  return axios
    .post(url, data)
    .then((res) => {
      console.log({ res });
      if (res?.data) return true;
      else return false;
    })
    .catch((er) => {
      console.log({ er });

      return false;
    });
};
export default globalPostApi;
