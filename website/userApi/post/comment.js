import axios from "../userBaseUrl";

const comment = async (url, data) => {
  return axios
    .post(url, data)
    .then((res) => ({ data: res?.data }))
    .catch((er) => false);
};
export default comment;
