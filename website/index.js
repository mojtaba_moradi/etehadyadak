import * as userApi from "./userApi";
import values from "./values";
import utils from "./utils";
const website = { userApi, values, utils };
export default website;
