const AddButton = ({ click, title, color, small }) => {
  return (
    <button onClick={click ? click : null} className={`btn-shopping ${color} ${small ? "small" : ""}`}>
      {title}
    </button>
  );
};

export default AddButton;
