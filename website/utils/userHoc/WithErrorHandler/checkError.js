const checkError = ({ errorCode, statusCode }) => {
  let errorTitle;
  console.log({ errorCode, statusCode });

  switch (errorCode) {
    case 1096:
      errorTitle = "آیدی اشتباه است";
      break;
    // case 1098:
    //   errorTitle = "پسورد شما نامعتبر است";
    //   break;
    // MOJTABA YADETE KHODET GOFTI
    case 1099:
      errorTitle = "خطایی در سرور . لطفا دوباره تلاش کنید";
      break;
    case 1019:
      errorTitle = "ارسال موفقیت آمیز نبود";
      break;
    case 1098:
      errorTitle = "اطلاعات ارسالی نامعتبر است";
      break;

    case 1017:
      errorTitle = "نام انگلیسی تکراری می باشد";
      break;
    case 1018:
      errorTitle = "نام فارسی تکراری می باشد";
      break;
    case 1020:
      errorTitle = "شماره تلفن ثبت نشده است";
      break;
    case 1013:
      errorTitle = "toMany ATTEMOP";
      break;

    default:
      switch (statusCode) {
        case 404:
          errorTitle = "این روت وجود ندارد";

          break;

        default:
          errorTitle = "خطا در سرور . لطفا دوباره تلاش کنید";

          break;
      }

      break;
  }
  return errorTitle;
};

export default checkError;
