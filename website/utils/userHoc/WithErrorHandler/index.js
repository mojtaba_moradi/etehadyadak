import React, { useState, useEffect } from "react";
import axios from "../../../userApi/userBaseUrl";
import checkError from "./checkError";
import toastify from "../../../../panelAdmin/utils/toastify";

const WithErrorHandler = (WrappedComponent) => {
  return (props) => {
    const [error, setError] = useState(null);
    const reqInterceptor = axios.interceptors.request.use((req) => {
      // console.log({ req });
      setError(null);
      return req;
    });
    const resInterceptor = axios.interceptors.response.use(
      (res) => {
        // console.log({ res });
        return res;
      },
      (err) => {
        setError(err);
      }
    );
    useEffect(() => {
      return () => {
        axios.interceptors.request.eject(reqInterceptor);
        axios.interceptors.response.eject(resInterceptor);
      };
    }, [reqInterceptor, resInterceptor]);
    console.log({ error });
    const test = error?.config?.url.split("/");
    console.log({ test });
    console.log({ error });

    useEffect(() => {
      // alert("error");
      if (error)
        if (test[3] !== "" && !test[4])
          if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
          else
            toastify(
              checkError({
                errorCode: error?.response?.data?.CODE,
                statusCode: error?.response?.status,
              }),
              "error"
            );
    }, [error]);

    return (
      <>
        <WrappedComponent {...props} />
      </>
    );
  };
};

export default WithErrorHandler;
