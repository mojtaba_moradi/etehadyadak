import swal from "sweetalert";
import website from "../..";
import UsersComments from "./UsersComments";
import ButtonComponent from "../../component/ui/ButtonComponent";
import AddButton from "../../component/ui/AddButton";
import LoginModal from "../../baseComponent/LoginModal";
import { useState, useEffect, useContext } from "react";
import Star from "../../../panelAdmin/component/UI/Rating/Star";
import reducer from "../../../_context/reducer";

const Comment = ({ data, parentType }) => {
  console.log({ comment: data });
  // ========================================================================== Context
  const websiteContext = reducer.webSiteReducer;
  // ================================== Authentication
  const authentication = useContext(websiteContext.authenticationReducerContext);
  const { dispatch: authenticationDispatch, state: authenticationState } = authentication;
  // ========================================================================== End Context
  // ========================================================================== State
  const [newComment, setNewComment] = useState({ name: "", content: "", rate: "2.5", parentType: parentType, parent: data?._id });
  const [showModal, setShowModal] = useState(false);
  // ========================================================================== End State
  // ========================================================================== Functions
  const changeStateValue = ({ name, value }) => {
    setNewComment({ ...newComment, [name]: value });
  };
  const submitNewComment = async (e) => {
    e.preventDefault();
    if (await website.userApi.post.globalPostApi(website.values.apiString.COMMENT, newComment)) {
      swal({
        title: "نظر شما با موفقیت ثبت شد!",
        text: "کارشناسان در حال بررسی پیامتان هستند بعد از تایید نظر شما ثبت خواهد شد!",
        icon: "success",
        button: "تایید",
      });
      setNewComment({ name: "", content: "", rate: "2.5", parentType: parentType, parent: data?._id });
    }
  };
  // ========================================================================== End Functions

  return (
    <div className="product-description-review">
      <form className="review-form1">
        <div>
          {data?.comments?.length ? (
            <>
              {" "}
              <h3 className="users-comments-subtitle">{"نظرات کاربران"}</h3>
              <UsersComments {...{ data }} />{" "}
            </>
          ) : (
            ""
          )}
          <h4 className="users-add-comment-subtitle">{"نظر خود را با ما به اشتراک بگذارید"}</h4>
          {authenticationState.authentic ? (
            <form onSubmit={submitNewComment} className="review-form2" action="review">
              <div className="show-review">
                <label htmlFor="name">{"نام شما"}</label>
                <input placeholder={"نام شما"} value={newComment.name} required id="name" type="" onChange={(e) => changeStateValue({ value: e.target.value, name: "name" })} />
                <label htmlFor="message">{"متن نظر شما"}</label>
                <textarea placeholder={"متن نظر شما"} value={newComment.content} onChange={(e) => changeStateValue({ value: e.target.value, name: "content" })} required id="message" cols="30" rows="10"></textarea>
              </div>
              <div className="rating-section">
                <label>امتیاز</label>
                <div className="star-rating-wrapper">
                  <Star rating={newComment.rate} change={(rate) => changeStateValue({ value: rate, name: "rate" })} />
                </div>
              </div>
              {/* <button>ارسال</button> */}
              <div className="textEnd">
                <AddButton title={"ثبت نظر شما"} color={"orange"} />
              </div>
            </form>
          ) : (
            <div className="user-check-active-road">
              <span>{"برای ارسال نظر باید اول ثبت نام کرده یا به پنل کاربری خود وارد شوید !!"}</span>
              <ButtonComponent title={"ورود / ثبت نام"} bgColor={"green"} style={null} click={() => setShowModal(true)} />
            </div>
          )}
        </div>
      </form>
      <LoginModal show={showModal} setShow={setShowModal} />
    </div>
  );
};

export default Comment;
