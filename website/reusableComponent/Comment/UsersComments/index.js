import jalaliDateConvert from "../../../../panelAdmin/utils/jalaliDateConvert";
import Star from "../../../../panelAdmin/component/UI/Rating/Star";

const UsersComments = ({ data }) => {
  let elementData = data?.comments?.map((comment, index) => {
    // console.log({ comment: jalaliDateConvert(comment?.createdAt) });
    let dateAndClock = jalaliDateConvert(comment?.createdAt, true);
    return (
      <div key={comment?.createdAt} className="show-comments-wrapper">
        <span className="textEnd comment-date">
          {" "}
          {dateAndClock.clock} - {dateAndClock.date}
        </span>
        <div className="comment-wrapper">
          <div>
            <span> {comment?.name}</span>

            <div className="comment-rate">
              <Star rating={comment?.rate} fixed />
            </div>
          </div>
          <div className="show-comment-text">
            <p>{comment?.content}</p>
          </div>
        </div>
      </div>
    );
  });
  return elementData || true;
};

export default UsersComments;
