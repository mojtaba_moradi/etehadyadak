import AboutUs from '../website/screens/AboutUs';

const AboutPage = () => {
  return <AboutUs />;
};
AboutPage.websiteLayout = true;

export default AboutPage;
