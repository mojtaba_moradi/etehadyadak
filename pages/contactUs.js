import ContactUs from '../website/screens/ContactUs';

const ContactPage = () => {
  return <ContactUs />;
};

ContactPage.websiteLayout = true;

export default ContactPage;
