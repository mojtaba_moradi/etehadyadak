import React, { useEffect, useContext } from "react";
import panelAdmin from "../../panelAdmin";
import reducer from "../../_context/reducer";
import AddAdmin from "../../panelAdmin/screen/Admin/AddAdmin";

const addAdmin = (props) => {
  const Context = reducer.panelAdminReducer.optionReducerContext;

  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;

  useEffect(() => {
    dispatch.changePageName(panelAdmin.values.fa.ADD_ADMIN);
  }, []);
  return <AddAdmin />;
};
addAdmin.panelAdminLayout = true;
// addAdmin.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   return { isServer };
// };

export default addAdmin;
