import React, { useEffect, useContext, useState } from "react";
import reducer from "../../_context/reducer";
import panelAdmin from "../../panelAdmin";
import useSWR, { mutate, trigger } from "swr";
import useApiRequest from "../../lib/useApiRequest";
import globalUtils from "../../globalUtils";
// import sliderScreen from "../../panelAdmin/screen/slider/sliderScreen";
import SpinnerRotate from "../../panelAdmin/component/UI/Loadings/SpinnerRotate";
import SliderScreen from "../../panelAdmin/screen/Slider/SliderScreen";

// const axios = globalUtils.axiosBase;
// const fetcher = (url) => axios(url).then((r) => r.json());

const slider = (props) => {
  const strings = panelAdmin.values.apiString;
  const Context = reducer.panelAdminReducer.optionReducerContext;
  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;
  const { acceptedCardInfo, parentTrue, isServer } = props;
  const [state, setState] = useState(false);
  const CurrentPage = state?.page || "1";
  const [loadingApi, setLoadingApi] = useState(true);
  // let parentType = state?.docs?.length && state?.docs[0]?.parentType;
  const [parentType, setParentType] = useState("Product");
  // ======================================================== SWR
  // const { data } = useSWR(strings.IMAGE, fetcher, { initialData: resData });
  // const { data: image } = useApiRequest(strings.IMAGE + "/" + CurrentPage, { initialData: resData }, { refreshInterval: 0 });
  useEffect(() => {
    dispatch.changePageName("اسلایدر");
    apiPageFetch();
  }, []);
  let resTitle;
  if (!state && loadingApi) resTitle = "در حال بارگزاری اطلاعات ...";
  else if (!state?.docs?.length) resTitle = "موردی یافت نشد ...";
  // //console.log({ data, resData });

  const apiPageFetch = async (page = CurrentPage, type = parentType) => {
    if (type) setParentType(type);
    if (!page) return;
    setLoadingApi(true);
    const res = await panelAdmin.api.get.sliders(page, type);
    //console.log({ res });
    setState(res?.data);
    setLoadingApi(false);
  };

  return (
    <>
      <SliderScreen {...{ parentType, resTitle, acceptedCardInfo, apiPageFetch }} requestData={state} />
      {loadingApi ? (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      ) : (
        ""
      )}
    </>
  );
  // return true;
};
// ========================================= getInitialProps
// slider.getInitialProps = async (props) => {
//   const { slider, isServer } = props.ctx;
//   const res = await panelAdmin.api.get.slider({ page: "1" });
//   const resData = res.data;

//   return { resData, isServer };
// };
slider.panelAdminLayout = true;

export default slider;
