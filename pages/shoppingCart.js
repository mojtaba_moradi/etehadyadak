import { useState, useContext } from "react";
import CartMainContent from "../website/screens/CartScreen/MainContent";
import ModalBox from "../panelAdmin/component/UI/Modals/ModalBox";
import ModalContent from "../website/screens/CartScreen/ModalContetnt";
import reducer from "../_context/reducer";

const ShoppingCart = () => {
  const Context = reducer.webSiteReducer.shoppingCartReducerContext;
  const giveContextData = useContext(Context);
  const { dispatch, state } = giveContextData;
  const [showModal, setShowModal] = useState(false);

  const showModalHandler = () => {
    setShowModal(!showModal);
  };
  return (
    <React.Fragment>
      <CartMainContent {...{ dispatch, state }} click={showModalHandler} />
      <ModalBox showModal={showModal} onHideModal={() => setShowModal(false)} animationOff={true}>
        <ModalContent />
      </ModalBox>
    </React.Fragment>
  );
};

ShoppingCart.websiteLayout = true;
export default ShoppingCart;
