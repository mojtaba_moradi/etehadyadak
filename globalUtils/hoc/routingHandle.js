import React from "react";
import PanelScreen from "../../panelAdmin/screen/PanelScreen";
import WebsiteScreen from "../../website/screens/WebsiteScreen";
const routingHandle = ({ Component, pageProps, router }) => {
  // console.log({ Component, pageProps, router });

  if (Component.panelAdminLayout || router?.asPath === "/panelAdmin/")
    return (
      <PanelScreen>
        <Component {...pageProps} />
      </PanelScreen>
    );
  else if (Component.websiteLayout) {
    return (
      <WebsiteScreen>
        <Component {...pageProps} />
      </WebsiteScreen>
    );
  } else return <Component {...pageProps} />;
};

export default routingHandle;
