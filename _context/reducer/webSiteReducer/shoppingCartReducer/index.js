import contextParent from "../../..";
// ========================================================  FUNCTIONS
// =============================  LOCAL_STORAGE
function localStorageSetItem({ param }) {
  localStorage.setItem("pernyBasket", JSON.stringify(param));
}
// =============================  ADD_PRODUCT
function handleAddProduct({ state, action }) {
  totalGoods();
  let returnData = { ...state, shoppingCart: [...state.shoppingCart, { ...action.payload, ["orderNumber"]: 1 }] };
  localStorageSetItem({ param: returnData });
  return returnData;
}
// =============================  REMOVE_PRODUCT
function handleRemoveProduct({ state, id }) {
  let returnData;
  let newState = state?.shoppingCart?.filter((basket) => {
    if (basket?._id !== id) return basket;
  });
  returnData = { ...state, shoppingCart: newState };
  localStorageSetItem({ param: returnData });
  return returnData;
}
// =============================  PRICE_INCREASE
function handleIncreaseOrder({ state, id }) {
  totalGoods();
  let returnData;
  let increaseOrderData = state?.shoppingCart?.map((basket) => {
    if (basket?._id == id) basket["orderNumber"] = basket?.orderNumber + 1 || 1;
    return basket;
  });
  returnData = { ...state, shoppingCart: increaseOrderData };
  localStorageSetItem({ param: returnData });
  return returnData;
}
// =============================  PRICE_DECREASE
function handleDecreaseOrder({ state, id }) {
  totalGoods();
  let returnData;
  let decreaseOrderData = state?.shoppingCart?.map((basket) => {
    if (basket?._id == id) basket["orderNumber"] = basket?.orderNumber > 1 ? basket?.orderNumber - 1 || 1 : 1;
    return basket;
  });
  returnData = { ...state, shoppingCart: decreaseOrderData };
  localStorageSetItem({ param: returnData });
  return returnData;
}
// =============================  TOTAL_PRICE
function handleTotalPrice({ state }) {
  let returnData;
  let totalPriceData;
  if (state?.shoppingCart?.length < 2) totalPriceData = (state?.shoppingCart[0]?.newPrice || state?.shoppingCart[0]?.realPrice) * (state?.shoppingCart[0]?.orderNumber || 1);
  else if (state?.shoppingCart?.length >= 2)
    totalPriceData = state.shoppingCart.reduce((accumulator, currentValue) => {
      let newAccumulator = (accumulator?.newPrice || accumulator?.realPrice || accumulator) * (accumulator?.orderNumber || 1);
      let newCurrentValue = (currentValue?.newPrice || currentValue?.realPrice) * (currentValue?.orderNumber || 1);
      return newAccumulator + newCurrentValue;
    });
  returnData = { ...state, ["totalPrice"]: totalPriceData };
  localStorageSetItem({ param: returnData });
  return returnData;
}
// =============================  TOTAL_GOODS
function handleTotalGoods({ state }) {
  let returnData;
  let totalGoods;
  if (state?.shoppingCart?.length < 2) totalGoods = state?.shoppingCart[0]?.orderNumber;
  else if (state?.shoppingCart?.length >= 2)
    totalGoods = state.shoppingCart.reduce((accumulator, currentValue) => {
      let newAccumulator = accumulator?.orderNumber || accumulator;
      let newCurrentValue = currentValue?.orderNumber;
      return newAccumulator + newCurrentValue;
    });
  returnData = { ...state, ["totalGoods"]: totalGoods };
  localStorageSetItem({ param: returnData });
  return returnData;
}
// ======================================================== END FUNCTIONS
// ========================================================  REDUCER
export const shoppingCartReducer = (state, action) => {
  switch (action.type) {
    case "NEW_STATE":
      return { ...state, ...action.payload };
    case "ADD_PRODUCT":
      return handleAddProduct({ state, action });
    case "REMOVE_PRODUCT":
      return handleRemoveProduct({ state, id: action.payload });
    case "INCREASE_ORDER_NUMBER":
      return handleIncreaseOrder({ state, id: action.payload });
    case "DECREASE_ORDER_NUMBER":
      return handleDecreaseOrder({ state, id: action.payload });
    case "TOTAL_PRICE":
      return handleTotalPrice({ state });
    case "TOTAL_GOODS":
      return handleTotalGoods({ state });
    default:
      return state;
  }
};
// ========================================================  DISPATCH
const newState = (dispatch) => (param) => {
  dispatch({ type: "NEW_STATE", payload: param });
};
const addProduct = (dispatch) => (data) => {
  dispatch({ type: "ADD_PRODUCT", payload: data });
};
const removeProduct = (dispatch) => (id) => {
  dispatch({ type: "REMOVE_PRODUCT", payload: id });
};
const increaseOrder = (dispatch) => (id) => {
  dispatch({ type: "INCREASE_ORDER_NUMBER", payload: id });
};
const decreaseOrder = (dispatch) => (id) => {
  dispatch({ type: "DECREASE_ORDER_NUMBER", payload: id });
};
const totalPrice = (dispatch) => () => {
  dispatch({ type: "TOTAL_PRICE" });
};
const totalGoods = (dispatch) => () => {
  dispatch({ type: "TOTAL_GOODS" });
};
export const { Provider, Context } = contextParent(
  shoppingCartReducer,
  {
    addProduct,
    removeProduct,
    increaseOrder,
    decreaseOrder,
    newState,
    totalPrice,
    totalGoods,
  },
  {
    shoppingCart: [],
    totalPrice: 0,
    totalGoods: 0,
  }
);
