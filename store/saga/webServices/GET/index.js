import { homeData } from "./homeData";
import { galleryData } from "./galleryApi";
import { artistData, artistSearchData } from "./artists";
const GET = {
  homeData,
  galleryData,
  artistData,
  artistSearchData,
};

export default GET;
