import { combineReducers } from "redux";
import errorReducer from "./errorReducer";
import themeColorReducer from "./themeColorReducer";
import panelAdmin from "../../panelAdmin";
import website from "../../website";
import cartReducer from "./cartReducer";
const rootReducer = combineReducers({
  error: errorReducer,
  themeColor: themeColorReducer,
  ...panelAdmin.reducer,
  ...website.reducer,
  cart: cartReducer,
});

export default rootReducer;
