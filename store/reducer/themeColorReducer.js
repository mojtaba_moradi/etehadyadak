import atRedux from "../actionTypes/redux";

export const webThemeColorInitialstate = {
  light: { primaryColor: "#0070ef", accentColor: "#666666", mainColor: "#ffff", titleColor: "#333333" },
  dark: { primaryColor: "#0070ef", accentColor: "#666666", mainColor: "#000", titleColor: "#ffff" },
  currentTheme: "light",
};

export const setThemeColor = (state, action) => {
  return { ...state, homeData: action.data };
};

function themeColorReducer(state = webThemeColorInitialstate, action) {
  //   switch (action.type) {
  //     case atRedux.:
  //       return setThemeColor(state, action);
  //     default:
  //       return state;
  //   }
  return state;
}

export default themeColorReducer;
