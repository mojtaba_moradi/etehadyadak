import React, { Component } from "react";
class LazyImage2 extends Component {
  // the following 2 lines will be used to solve the race condition problem
  loadTimer;
  image = new Image();

  componentDidMount() {
    const { loaderIcon } = this.props;

    // the following 2 lines to solve the "race condition" problem
    this.image.src = loaderIcon ? loaderIcon : "https://www.eliananunes.com/images/lazy_loader.gif";
    this.image.addEventListener("load", this.onImgLoaded);

    // add the lazyLoad method onscroll
    window.onscroll = window.addEventListener("scroll", this.lazyLoad);
  }

  componentWillUnmount() {
    // remove the lazyLoad method
    window.removeEventListener("scroll", this.lazyLoad);
  }

  onImgLoaded = () => {
    if (this.loadTimer !== null) {
      clearTimeout(this.loadTimer);
    }

    if (!this.image.complete) {
      this.loadTimer = setTimeout(() => {
        this.onImgLoaded();
      }, 3);
    } else {
      this.lazyLoad();
    }
  };

  lazyLoad = (e) => {
    Object.values(this.refs).forEach((el) => {
      if (this.elementInViewPort(el)) {
        el.setAttribute("src", el.getAttribute("data-src"));
      }
    });
  };

  elementInViewPort(el) {
    // getBoundingClientRect => returns the size of the given element and the position of it in relation to the view port
    const clientRect = el.getBoundingClientRect();

    return clientRect.top >= 0 && clientRect.left >= 0 && clientRect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && clientRect.right <= (window.innerWidth || document.documentElement.clientWidth);
  }

  render() {
    const { images, loaderIcon } = this.props;

    return (
      <Fragment>
        {images.map((el, i) => {
          return <img ref={"image" + i} src={loaderIcon ? loaderIcon : "https://www.eliananunes.com/images/lazy_loader.gif"} data-src={el} key={i} />;
        })}
      </Fragment>
    );
  }
}

class App extends Component {
  images = [
    "https://picsum.photos/id/736/300/300",
    "https://picsum.photos/id/730/300/300",
    "https://picsum.photos/id/635/300/300",
    "https://picsum.photos/id/536/300/300",
    "https://picsum.photos/id/436/300/300",
    "https://picsum.photos/id/336/300/300",
    "https://picsum.photos/id/236/300/300",
    "https://picsum.photos/id/136/300/300",
    "https://picsum.photos/id/74/300/300",
  ];

  render() {
    return (
      <div className="container">
        <h1>Scroll down to see images</h1>
        {/*LazyImage properties:
                - images => an array of the required images
                - loaderIcon => the source of the loader icon*/}
        <LazyImage images={this.images} loaderIcon="http://lamama.org/wp-content/plugins/gallery-by-supsystic/src/GridGallery/Galleries/assets/img/loading.gif" />
      </div>
    );
  }
}

export default LazyImage2;
