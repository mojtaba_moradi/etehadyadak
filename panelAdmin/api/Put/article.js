import axios from "../axios-orders";
import panelAdmin from "../..";

const article = async (param) => {
  const toastify = panelAdmin.utils.toastify;
  const URL = panelAdmin.values.apiString.BLOG;

  return axios
    .put(URL + "/" + param.id, param.data)
    .then((Response) => {
      // console.log({ Response });
      toastify("با موفقیت ثبت شد", "success");
      return true;
    })
    .catch((error) => {
      // console.log({ error });
      return false;
    });
};
export default article;
