import axios from "../axios-orders";
import panelAdmin from "../..";

const banner = async (param) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;
  let URL = strings.BANNER;

  return axios
    .post(URL, param)
    .then((Response) => {
      console.log({ Response });
      toastify("با موفقیت ثبت شد", "success");
      return Response;
    })
    .catch((error) => {
      console.log({ error });

      // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      // else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default banner;
