import axios from "../axios-orders";
import panelAdmin from "../..";

const contactUs = async (param, id) => {
  const toastify = panelAdmin.utils.toastify;
  let URL = panelAdmin.values.apiString.CONTACT_US + "/" + id;
  return axios
    .post(URL, param)
    .then((Response) => {
      // console.log({ Response });
      toastify("با موفقیت ثبت شد", "success");
      return true;
    })
    .catch((error) => {
      console.log({ error });
      return false;
    });
};
export default contactUs;
