import product from "./product";
import gallery from "./gallery";
import category from "./category";
import owner from "./owner";
import store from "./store";
import slider from "./slider";
import banner from "./banner";
import blogCategory from "./blogCategory";
import article from "./article";
import comment from "./comment";

const Delete = { comment, article, blogCategory, product, gallery, category, owner, store, slider, banner };
export default Delete;
