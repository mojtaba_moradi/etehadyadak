import axios from "../axios-orders";
import panelAdmin from "../..";

const products = async (page) => {
  const strings = panelAdmin.values.apiString.PRODUCT;
  let URL = page ? strings + "?page=" + page : strings;
  return axios
    .get(URL)
    .then((products) => {
      console.log({ products });
      return products?.data;
    })
    .catch((error) => {
      // console.log({ error });
      return false;
    });
};

export default products;
