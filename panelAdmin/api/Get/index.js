import categories from "./categories";
import products from "./products";
import productSearch from "./productSearch";
import gallery from "./gallery";
import sliders from "./sliders";
import banners from "./banners";
import blogCategory from "./blogCategory";
import articles from "./articles";
import articleSearch from "./articleSearch";
import comments from "./comments";
import contactUs from "./contactUs";

const get = {
  categories,
  products,
  productSearch,
  gallery,
  sliders,
  banners,
  blogCategory,
  articles,
  articleSearch,
  comments,
  contactUs,
};
export default get;
