import axios from "../axios-orders";
import panelAdmin from "../..";

const productSearch = async (param, page = 1) => {
  //console.log({ param, page });

  const strings = panelAdmin.values.apiString.PRODUCT + "?page=" + page + "&search=" + param;
  //console.log({ strings });

  return axios
    .get(strings)
    .then((productSearch) => {
      console.log({ productSearch });

      return productSearch?.data;
    })
    .catch((error) => {
      // console.log({ error });
      // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      // //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default productSearch;
