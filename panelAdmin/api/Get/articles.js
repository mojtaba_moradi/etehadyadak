import axios from "../axios-orders";
import panelAdmin from "../..";

const articles = async (page) => {
  const strings = panelAdmin.values.apiString;
  let getUrl = page ? strings.BLOG + "?page=" + page : strings.BLOG;

  return axios
    .get(getUrl)
    .then((res) => {
      // console.log({ res });
      return res?.data;
    })
    .catch((error) => {
      // console.log({ error });
      return false;
    });
};

export default articles;
