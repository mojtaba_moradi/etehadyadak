import panelAdmin from "../../..";

const banner = (data, acceptedCard) => {
  const cardFormat = [];
  //console.log({ insss: data });

  for (let index in data) {
    let noEntries = "وارد نشده است";
    let dataIndex = data[index];
    let parent = dataIndex?.parent || false;
    let parentName = parent?.name || noEntries;

    let parentType = dataIndex?.parentType || false;
    let image = dataIndex.image ? dataIndex.image : "";

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? (acceptedCard === image ? "activeImage" : "") : "",
      image: { value: image },
      body: [
        {
          right: [{ elementType: "text", value: parentName, style: { color: "#828181", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
        {
          left: [{ elementType: "text", value: panelAdmin.utils.dictionary(parentType), style: { color: "#147971", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
      ],
    });
  }
  return cardFormat;
};

export default banner;
