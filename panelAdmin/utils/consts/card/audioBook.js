import React from "react";
import PanelString from "../../../value/PanelString";

const audioBook = (data) => {
  const cardFormat = [];
  let NotSelected = "انتخاب نشده";

  for (let index in data) {
    let title = data[index].title ? data[index].title : NotSelected;
    let description = data[index].description ? data[index].description : NotSelected;
    let price = data[index].price ? data[index].price : NotSelected;
    // let publisher = data[index].publisher.title ? data[index].publisher.title : NotSelected;

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: { value: data[index].cover },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "1.3em", fontWeight: "bold" } }],
        },
        {
          right: [{ elementType: "text", value: description, title: description, style: { color: PanelString.color.GRAY, fontSize: "1em", fontWeight: "500" } }],
        },
        {
          // right: [{ elementType: "text", value: publisher }],
          left: [{ elementType: "price", value: price, direction: "ltr" }],
        },
      ],
    });
  }
  return cardFormat;
};

export default audioBook;
