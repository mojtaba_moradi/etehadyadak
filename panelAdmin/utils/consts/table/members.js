import React from "react";

const members = (data) => {
  const thead = ["#", "نام", "شماره همراه", "تراکنش ها", "تخفیف ها", "وضعیت"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let active = <i style={{ fontSize: "1em", color: "green", fontWeight: "900" }} className="far fa-check-circle"></i>;
    let deActive = <i style={{ fontSize: "1em", color: "red", fontWeight: "900" }} className="fas fa-ban"></i>;

    // let avatar = data[index].avatar ? data[index].avatar : NotEntered;
    let name = data[index].name ? data[index].name : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let showTransaction = { option: { eye: true, name: "transactions" } };
    let showDiscount = { option: { eye: true, name: "discounts" } };
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({ data: [name, phoneNumber, showTransaction, showDiscount, isActive], style: {} });
  }
  return [thead, tbody];
};

export default members;
