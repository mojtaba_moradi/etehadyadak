import React from "react";
import jalaliDateConvert from "../../jalaliDateConvert";
import dictionary from "../../dictionary";

const articles = (data) => {
  console.log({ data });
  const thead = ["#", "نام ", "شماره همراه", "عنوان ", "امتیاز", "تاریخ", "نظر", "تنظیمات"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let userInfo = data[index]?.user;
    let productInfo = data[index]?.parent;
    let userName = data[index]?.name || NotEntered;
    let userPhoneNumber = userInfo?.phoneNumber || NotEntered;
    let productName = productInfo?.name || productInfo?.title || NotEntered;
    let rate = { option: { star: true, value: data[index]?.rate } };
    let content = { option: { eye: true, name: "content" } };
    let parentType = dictionary(data[index]?.parentType || NotEntered);
    let jalali = jalaliDateConvert(data[index]?.createdAt);
    let jalaliDate = data[index]?.createdAt ? jalali.clock + " - " + jalali.date : noEntries;
    let isConfirmed = data[index]?.isConfirmed;
    tbody.push({ data: [userName, userPhoneNumber, productName, rate, jalaliDate, content, { option: { remove: true, accept: !isConfirmed ? true : false } }], style: {} });
  }
  return { thead, tbody };
};

export default articles;
