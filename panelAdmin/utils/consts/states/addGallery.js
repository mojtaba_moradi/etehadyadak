const addGallery = {
  Form: {
    imageType: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد",
      },
      childValue: [
        { name: "دسته بندی", value: "category" },
        { name: "محصول", value: "product" },
        { name: "اسلایدر", value: "slider" },
        { name: "مقاله", value: "blog" },
      ],
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    imageName: {
      label: "نام عکس :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام عکس",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    image: {
      label: " عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addGallery;
