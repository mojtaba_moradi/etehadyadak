const addContactUs = {
  Form: {
    title: {
      label: "توضیحات  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات ",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    phoneNumber: {
      label: "تلفن همراه :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "تلفن همراه",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: true,
      touched: false,
    },
    instagramAddress: {
      label: "اینستاگرام :",
      elementType: "input",
      elementConfig: {
        placeholder: "اینستاگرام",
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
    telegramAddress: {
      label: "تلگرام :",
      elementType: "input",
      elementConfig: {
        placeholder: "تلگرام",
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
    address: {
      label: "آدرس :",
      elementType: "textarea",
      elementConfig: {
        placeholder: "آدرس",
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addContactUs;
