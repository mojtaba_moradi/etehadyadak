import Cookie from "js-cookie";
const authorization = () => {
  return Cookie.get("PernyAdminToken") !== undefined;
};
export default authorization;
