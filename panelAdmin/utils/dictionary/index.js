const dictionary = (text) => {
  let translated;
  let lowerText = text.toLowerCase();
  switch (lowerText) {
    case "product":
      translated = "محصول";
      break;
    case "category":
      translated = "دسته بندی";
      break;
    case "blog":
      translated = "مقاله";
      break;
    default:
      translated = text;
      break;
  }
  return translated;
};

export default dictionary;
