import panelAdmin from "../..";

const formTouchChange = ({ data, setData }) => {
  const updateObject = panelAdmin.utils.updateObject;
  const checkValidity = panelAdmin.utils.checkValidity;
  let changeValid,
    updatedForm,
    updatedFormElement = {};
  for (const key in data.Form) {
    let value = data.Form[key].value;
    let typeofData = typeof value;
    let isArray, isObject, isString;

    typeofData === "object" && value != undefined ? (value && value.length >= 0 ? (isArray = true) : (isObject = true)) : (isString = true);
    ////console.log({ data }, isArray, isObject, isString);
    updatedFormElement[key] = updateObject(data.Form[key], {
      touched: true,
      titleValidity: checkValidity(data.Form[key].value, data.Form[key].validation, isArray, data.Form[key].value).errorTitle,
    });
  }
  updatedForm = updateObject(data.Form, { ...updatedFormElement });
  return setData({ Form: updatedForm, formIsValid: false });
};

export default formTouchChange;
