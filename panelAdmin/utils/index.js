import consts from "./consts";
import toastify from "./toastify";
import CelanderConvert from "./CelanderConvert";
import onChanges from "./onChanges";
import handleKey from "./handleKey";
import formatMoney from "./formatMoney";
import json from "./json";
import operation from "./operation";
import authorization from "./authorization";
import { checkValidity } from "./checkValidity";
import updateObject from "./updateObject";
import adminHoc from "./adminHoc";
import dictionary from "./dictionary";
import jalaliDateConvert from "./jalaliDateConvert";
import timeNow from "./timeNow";

const utils = { timeNow, jalaliDateConvert, dictionary, adminHoc, updateObject, authorization, checkValidity, consts, toastify, CelanderConvert, onChanges, handleKey, formatMoney, json, operation };
export default utils;
