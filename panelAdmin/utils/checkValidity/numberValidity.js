const numberValidity = ({ array, beforeValue, value }) => {
  let isNumeric = true;
  const pattern = /^\d+$/;

  if (array)
    beforeValue.map((val) => {
      // ////console.log("number:", { val });

      return (isNumeric = pattern.test(val) && isNumeric);
    });
  else isNumeric = pattern.test(value) && isNumeric;

  return isNumeric;
};
export default numberValidity;
