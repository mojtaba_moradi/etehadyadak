import category from "./category.json";
import gallery from "./gallery.json";

const table = { category, gallery };
export default table;
