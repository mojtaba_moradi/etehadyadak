import panelAdmin from "../..";

export const uploadInitialState = {
  uploadImageData: "",
};

export const setUploadImageData = (state, action) => {
  return { ...state, uploadData: action.data };
};

function uploadReducer(state = uploadInitialState, action) {
  const atRedux = panelAdmin.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_UPLOAD_IMAGE:
      return setUploadImageData(state, action);
    default:
      return state;
  }
}

export default uploadReducer;
