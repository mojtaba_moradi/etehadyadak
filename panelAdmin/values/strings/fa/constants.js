const ALBUM_CONSTANTS = "آلبوم";
const PLAY_LIST_CONSTANTS = "لیست پخش";
const SONG_CONSTANTS = "موزیک";
const FLAG_CONSTANTS = "پرچم";
const ARTIST_CONSTANTS = "هنرمند";
const INSTRUMENT_CONSTANTS = "ساز";
const MUSIC_VIDEO_CONSTANTS = "موزیک ویدئو";
const MOOD_CONSTANTS = "حالت";
const constants = {
  ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS,
  FLAG_CONSTANTS,
  ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS,
};

export default constants;
