import navbar from "./navbar.js";
import sideMenu from "./sideMenu";
import global from "./global.js";
import constants from "./constants";
const en = {
  ...navbar,
  ...sideMenu,
  ...global,
  ...constants,
};
export default en;
