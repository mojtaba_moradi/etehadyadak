const ADMIN = "/admin";
const CATEGORY = ADMIN + "/category";
const PRODUCT = ADMIN + "/product";
const IMAGE = ADMIN + "/image";
const UPLOAD = ADMIN + "/upload";
const SLIDER = ADMIN + "/slider";
const BANNER = ADMIN + "/banner";
const BLOG_CATEGORY = ADMIN + "/blogCategory";
const BLOG = ADMIN + "/blog";
const COMMENT = ADMIN + "/comment";
const LOGIN = ADMIN + "/login";
const CONTACT_US = ADMIN + "/contactus";

const apiString = {
  CATEGORY,
  PRODUCT,
  IMAGE,
  SLIDER,
  BANNER,
  UPLOAD,
  BLOG_CATEGORY,
  BLOG,
  COMMENT,
  LOGIN,
  CONTACT_US,
};
export default apiString;
