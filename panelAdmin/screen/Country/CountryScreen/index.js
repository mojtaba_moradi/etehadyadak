import React, { useEffect, useState, Fragment, useRef } from "react";
import { get } from "../../../api";
import SpinnerRotate from "../../../component/UI/Loadings/SpinnerRotate";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";
import { useSelector } from "react-redux";
import panelAdmin from "../../..";

const CountryScreen = (props) => {
  const { onDataChange, filters, acceptedCardInfo } = props;

  const [state, setState] = useState({ remove: { index: "", name: "" }, genreTitle: "" });
  const inputRef = useRef(null);
  const store = useSelector((state) => {
    return state.country;
  });
  const card = panelAdmin.utils.consts.card;
  const loading = store.loading;
  const searchLoading = store.searchLoading;
  const CountryData = store.countryData;
  const searchCountry = store.searchCountry;
  const CountrySearch = searchCountry ? (searchCountry.docs.length ? true : false) : false;
  ////console.log({ loading, searchLoading, CountryData, searchCountry, CountrySearch });

  ////console.log({ storeMoodScreen: store });

  ////console.log({ CountrySearch });
  const showDataElement = CountryData && <ShowCardInformation data={card.country(CountryData.docs)} onClick={null} optionClick={null} />;
  return (
    <React.Fragment>
      {showDataElement}
      {loading && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )}
    </React.Fragment>
  );
};

export default CountryScreen;
