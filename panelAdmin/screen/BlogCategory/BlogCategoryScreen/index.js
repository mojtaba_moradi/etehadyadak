import React, { useState } from "react";
import panelAdmin from "../../..";
import BlogCategoryTableElement from "./BlogCategoryTableElement";
import AddBlogCategory from "../AddBlogCategory";
import ModalStructure from "../../../component/UI/Modals/ModalStructure";
const BlogCategoryScreen = (props) => {
  const { apiPageFetch, onDataSearch, requestData, loadingApi } = props;
  const [removeLoading, setRemoveLoading] = useState(false);
  const [modalDetails, setModalDetails] = useState({
    show: false,
    kindOf: null,
    data: null,
    name: null,
    removeId: null,
    editId: null,
    editData: [],
  });
  console.log({ modalDetails });

  // ========================================================= remove data with data id
  const reqApiRemove = async (id) => {
    setRemoveLoading(true);
    if (await panelAdmin.api.deletes.blogCategory(id)) {
      apiPageFetch(requestData.page);
      onHideModal();
    }
    setRemoveLoading(false);
  };
  // ========================================================= End remove data with data id
  // ========================================================= modal
  // ================================== modal close
  const onHideModal = () => {
    setModalDetails({ ...modalDetails, show: false, kindOf: false, removeId: null });
  };
  // ================================== modal open
  const onShowModal = (event) => {
    setModalDetails({ ...modalDetails, show: true, kindOf: event.kindOf, name: event?.name, editData: event?.editData, removeId: event?.removeId });
  };
  // ================================== handel modal end work
  const modalRequest = async (bool) => {
    if (bool) reqApiRemove(modalDetails.removeId);
    else onHideModal();
  };
  // ========================================================= END modal

  // ========================================================= table handel icon click
  const tableOnclick = (index, kindOf) => {
    ////console.log({ index, kindOf });
    switch (kindOf) {
      case "remove":
        onShowModal({ kindOf: "question", removeId: requestData[index]._id });
        break;
      case "edit":
        onShowModal({ kindOf: "component", editData: requestData[index] });
        break;
      case "add":
        onShowModal({ kindOf: "component", editData: false });
        break;
      default:
        break;
    }
  };
  // ========================================================= End table handel click
  const searchData = (e) => {
    onDataSearch("", e.target.value);
  };
  return (
    <React.Fragment>
      <ModalStructure {...{ modalRequest, reqApiRemove, onHideModal, modalDetails }} loading={removeLoading}>
        {modalDetails.kindOf === "component" && <AddBlogCategory propsHideModal={onHideModal} setEdit={apiPageFetch} editData={modalDetails?.editData} modalAccept={modalRequest} />}
      </ModalStructure>
      <BlogCategoryTableElement requestData={requestData} handelPage={apiPageFetch} tableOnclick={tableOnclick} handelChange={searchData} />
    </React.Fragment>
  );
};

export default BlogCategoryScreen;
