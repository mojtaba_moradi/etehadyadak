import React from "react";
import updateObject from "../../../../../utils/updateObject";

const Submitted = async (props) => {
  const { setSubmitLoading, data, editData, put, post, setData, states, setEdit, propsHideModal, setCheckSubmitted, checkSubmitted } = props;
  setSubmitLoading(true);
  setCheckSubmitted(!checkSubmitted);
  const formData = {};
  for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
  const initialStateOwner = updateObject(states.addCategory.Form["owner"], { value: [] });
  const updatedForm = updateObject(states.addCategory.Form, { ["owner"]: initialStateOwner });
  if (editData) {
    if (await put.category({ id: editData._id, data: formData })) {
      setEdit();
      propsHideModal();
    }
  } else if (await post.category(formData)) setData({ ...states.addCategory });

  setSubmitLoading(false);
};

export default Submitted;
