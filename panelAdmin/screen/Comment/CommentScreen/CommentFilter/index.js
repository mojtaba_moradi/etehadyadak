import React from "react";
import _ from "lodash";
import DropdownBoot from "../../../../component/UI/Inputs/DropdownBoot";
const CommentFilter = ({ filters, onFilterClick, currentFilter, dropDownData }) => {
  return (
    <ul className="gallery-filter">
      <div className="btns-container">
        <DropdownBoot dropDownData={dropDownData} accepted={onFilterClick} value={currentFilter} />
      </div>
      {filters &&
        filters.map((filter, index) => {
          return (
            <li key={"gallery-filter-" + index} onClick={() => onFilterClick({ [filter.key]: filter.value })} className={filter.active ? "active" : ""}>
              {_.upperFirst(filter.title)}
            </li>
          );
        })}
    </ul>
  );
};

export default CommentFilter;
