import React from "react";

const EditData = (props) => {
  const { editData, stateArray, inputChangedHandler } = props;
  let arrayData = [];
  let newData = { ...editData };
  if (newData)
    for (const key in newData)
      for (let index = 0; index < stateArray.length; index++) {
        if (stateArray[index].id === key)
          if (key === "category") arrayData.push({ name: key, value: newData[key] ? newData[key]._id : "" });
          else arrayData.push({ name: key, value: newData[key] ? newData[key] : newData[key] ? newData[key] : "" });
      }
  console.log({ arrayData });
  if (arrayData.length > 0) inputChangedHandler(arrayData, false, { value: newData.parentType });
};

export default EditData;
