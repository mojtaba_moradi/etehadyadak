import React from "react";
import CardElement from "../CardElement";
import Scrollbar from "react-scrollbars-custom";
import PerfectScrollbar from "react-perfect-scrollbar";

const ShowCardInformation = ({ data, onClick, submitedTitle, optionClick, options, acceptedCardInfo }) => {
  const showDataAll = data?.map((information, index) => {
    // //////console.log({ information });
    return <CardElement {...{ options, optionClick, index, onClick, submitedTitle, acceptedCardInfo }} key={index + "mmm"} data={information} />;
  });
  const ShowData = <CardElement {...{ data, onClick, submitedTitle, optionClick, options, acceptedCardInfo }} />;

  return <div className="show-card-elements row m-0">{data.length > 0 ? showDataAll : ShowData}</div>;
};

export default ShowCardInformation;
