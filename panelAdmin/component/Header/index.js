import React, { useContext } from "react";
import HeaderProfile from "./HeaderProfile";
import reducer from "../../../_context/reducer";
const Context = reducer.panelAdminReducer.optionReducerContext;

const Header = ({ _handelSidebarToggle }) => {
  const giveContextData = useContext(Context);
  const { state } = giveContextData;
  return (
    <nav className="panelAdmin-navbar-container">
      <div className="panel-navbar-box">
        <div className="panel-navbar-side-element smallDisplay">
          <i onClick={_handelSidebarToggle} className="fas fa-bars"></i>
          <span className="page-accepted-name">{state.pageName}</span>
        </div>
        <div className="panel-navbar-side-element">
          <ul className="panel-navbar-notifications">
            <li className="pointer hoverColorblack normalTransition">
              <i className="icon-search"></i>
            </li>
            <li className="navbar-icon-massege pointer hoverColorblack normalTransition">{/* <div className="massege-icon">
                <i className="far fa-bell"></i>
                <span className="show-modal-icon-value">4</span>
              </div> */}</li>
          </ul>
          <HeaderProfile />
        </div>
      </div>
    </nav>
  );
};

export default Header;
