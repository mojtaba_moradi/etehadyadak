import React, { Fragment, useState, useEffect } from "react";
import { Table } from "react-bootstrap";
import Star from "../Rating/Star";
import Pageination from "../PaginationM";
import IsNull from "../IsNull";
import LazyImage from "../../../../components/LazyImage";
const TableBasic = (props) => {
  const { thead, tbody, onClick, imgStyle, subTitle, btnHead, pageination, searchHead, onChange, apiPage } = props;
  const [pageinat, setpageinat] = useState(false);
  ////console.log({ apiPage });

  useEffect(() => {
    if (pageination)
      setpageinat({
        total: tbody.length,
        limit: pageination.limited,
        page: "1",
        pages: Math.ceil(tbody.length / pageination.limited),
      });
    else setpageinat(false);
  }, []);
  let body = [];
  if (pageination) body = tbody.slice(pageinat.limit * pageinat.page - pageinat.limit, pageinat.limit * pageinat.page);
  else body = tbody;

  const _handelPage = (value) => {
    if (value) setpageinat({ ...pageinat, page: value });
  };
  const optionElement = (option, parentIndex) => {
    return (
      <Fragment>
        {option.accept && (
          <span>
            <i title="تایید " className="fad fa-check-circle" onClick={() => onClick(parentIndex, "accept")}></i>
          </span>
        )}
        {option.edit && (
          <span>
            <i title="ویرایش " className="far fa-edit" onClick={() => onClick(parentIndex, "edit")} />
          </span>
        )}
        {option.remove && (
          <span>
            <i title="حذف " className="fad fa-trash-alt" onClick={() => onClick(parentIndex, "remove")} />
          </span>
        )}
        {option.eye && (
          <span>
            <i title="مشاهده " className="fas fa-eye" onClick={() => onClick(parentIndex, "showModal", option.name)} />
          </span>
        )}
        {option.star && (
          <span>
            <Star rating={option.value} fixed size={"small"} />
          </span>
        )}
        {option.play && (
          <span>
            <i onClick={() => onClick(parentIndex, "play")} className="fal fa-play"></i>
          </span>
        )}
        {option.disable && (
          <span>
            <i title="مسدود " className="fad fa-minus-circle" onClick={() => onClick(parentIndex, "disable")}></i>
          </span>
        )}
      </Fragment>
    );
  };
  return (
    <Fragment>
      {/* {subTitle ||
        (searchHead && ( */}
      <div className="subtitle-elements">
        <span className="centerAll">{subTitle} </span>
        <div className="head-optional-elements" style={{ display: "flex" }}>
          {btnHead && (
            <div className="btns-container">
              <div>
                <button onClick={() => onClick(false, "add")} className="btns btns-primary">
                  {btnHead.title}
                </button>
              </div>
            </div>
          )}{" "}
          {searchHead && searchHead}
        </div>

        {/*  ))
        } */}
      </div>
      {body?.length ? (
        <div className="show-elements">
          <Table striped hover size="sm" className="table-wrapper">
            <thead>
              <tr>
                {thead &&
                  thead.map((infoHead, parentIndex) => {
                    return (
                      <th className="textCenter" key={parentIndex + "mor"}>
                        {infoHead}
                      </th>
                    );
                  })}
              </tr>
            </thead>

            <tbody>
              {body?.length &&
                body.map((infoBody, parentIndex) => {
                  // console.log({ infoBody });

                  return (
                    <tr style={infoBody.style} key={parentIndex + "mojiii"}>
                      {pageination ? <td className="textCenter">{pageinat.limit * pageinat.page - pageinat.limit + parentIndex + 1}</td> : (apiPage && <td className="textCenter">{apiPage.limit * apiPage.page - apiPage.limit + parentIndex + 1}</td>) || <td className="textCenter">{parentIndex + 1}</td>}
                      {/* {pageination ? <td className="textCenter">{pageinat.limit * pageinat.page - pageinat.limit + parentIndex + 1}</td> : <td className="textCenter">{apiPage.limit * apiPage.page - apiPage.limit + parentIndex + 1}</td>} */}
                      {infoBody.data.map((info, childIndex) => {
                        const pattern = RegExp("http");
                        return info ? (
                          pattern.test(info) ? (
                            <td key={childIndex + "mor"} className="textCenter">
                              <LazyImage style={imgStyle} src={info} />
                              {/* <img   alt="image" /> */}
                            </td>
                          ) : info.option ? (
                            <td className=" table-wrapper " key={childIndex + "mo"}>
                              {optionElement(info.option, parentIndex)}
                            </td>
                          ) : (
                            <td key={childIndex + "mojta"} className="textCenter">
                              {info}
                            </td>
                          )
                        ) : (
                          ""
                        );
                      })}
                    </tr>
                  );
                })}
            </tbody>
          </Table>
          <div className="centerAll width100">{pageination && pageinat.pages > 1 && <Pageination limited={"2"} pages={pageinat.pages} activePage={pageinat.page} onClick={_handelPage} />}</div>{" "}
        </div>
      ) : (
        ""
        // <IsNull title={"موردی یافت نشد"} />
      )}
    </Fragment>
  );
};

export default TableBasic;
