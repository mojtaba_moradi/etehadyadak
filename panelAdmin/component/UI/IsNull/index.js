import React from "react";
const IsNull = (props) => {
  const { title } = props;
  return (
    <div className="Null-data">
      <span>{title}</span>
    </div>
  );
};

export default IsNull;
