import React, { useRef } from "react";
const InputVsIcon = (props) => {
  const { icon, placeholder, name, onChange, inputRef, dir, value } = props;

  return (
    <div style={{ direction: "" }} className="input-icon-wrapper centerAll">
      <input name={name} placeholder={placeholder} ref={inputRef} onChange={onChange} autoComplete="off" value={value} />
      <i className={icon} />
    </div>
  );
};

export default InputVsIcon;
